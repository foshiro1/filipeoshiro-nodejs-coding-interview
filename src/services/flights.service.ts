import { FlightsModel } from '../models/flights.model'

export class FlightsService {
    getAll() {
        try {
            return FlightsModel.find()
        } catch (err) {
            console.log(err)
        }
    }

    async onboardPerson(flightId: string, personId: string) {
        // Validate if flight exists, given flight id
        // validate if person exists, given person id
        console.log({flightId, personId});
        try {
            await FlightsModel.updateOne({ _id: flightId }, 
                { $push: { passengers: personId } }
            );
        } catch(err) {
            throw err;
        }
    }
}
