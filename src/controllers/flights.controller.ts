import { JsonController, Get, Post, Param } from 'routing-controllers'
import { FlightsModel } from '../models/flights.model'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        try {
            const allFlights = await flightsService.getAll();
            
            return {
                status: 200,
                data: allFlights,
            }
        } catch( err) {
            return {
                status: 500,
                message: 'Error trying get all flights',
            }
        }
    }

    @Post('/:flightId/onboard/:personId', { transformResponse: false })
    async onboardPerson(@Param("flightId") flightId: string, @Param("personId") personId: string) {
        try {
            await flightsService.onboardPerson(flightId, personId);
            return {
                status: 200,
                message: 'Person onboarded',
            }
        } catch (err) {
            return {
                status: 500,
                message: 'Error trying to onboarde person',
            }
        }
    }
}
